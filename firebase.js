const initializeApp = require('firebase/app');
var admin = require("firebase-admin");
var serviceAccount = require("./keys.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://print-plugin-b98d4-default-rtdb.europe-west1.firebasedatabase.app"
  });

  var db = admin.database();

module.exports = {
    db
  };

