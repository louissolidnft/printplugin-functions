gallery:
	gcloud functions deploy printButtonGallery --runtime nodejs18 --trigger-http --allow-unauthenticated
unique:
	gcloud functions deploy printButtonUnique --runtime nodejs18 --trigger-http --allow-unauthenticated
webhook:
	gcloud functions deploy webhook --runtime nodejs18 --trigger-http --allow-unauthenticated 
checking:
	gcloud functions deploy checkSuccess --runtime nodejs18 --trigger-http --allow-unauthenticated

# PrintRestriction
newCollab:
	gcloud functions deploy newCollabPrintRestriction --runtime nodejs18 --trigger-http --allow-unauthenticated
updateCollab:
	gcloud functions deploy updateCollabPrintRestriction --runtime nodejs18 --trigger-http --allow-unauthenticated
checkCollab:
	gcloud functions deploy checkCollabPrintRestriction --runtime nodejs18 --trigger-http --allow-unauthenticated

# Shopify
newcustomer:
	gcloud functions deploy newCustomer --runtime nodejs18 --trigger-http --allow-unauthenticated
newproduct:
	gcloud functions deploy newProduct --runtime nodejs18 --trigger-http --allow-unauthenticated
neworder:
	gcloud functions deploy newOrder --runtime nodejs18 --trigger-http --allow-unauthenticated
updateorder:
	gcloud functions deploy updateOrder --runtime nodejs18 --trigger-http --allow-unauthenticated
newcheckout:
	gcloud functions deploy newCheckout --runtime nodejs18 --trigger-http --allow-unauthenticated
checkoutcreate:
	gcloud functions deploy checkoutCreate --runtime nodejs18 --trigger-http --allow-unauthenticated
createcart:
	gcloud functions deploy cartCreate --runtime nodejs18 --trigger-http --allow-unauthenticated
