const functions = require('@google-cloud/functions-framework');
const Stripe = require ('stripe');
const axios = require('axios');
const uuid = require('uuid');
const cors = require('cors');
const admin = require('firebase-admin');
const stripe = Stripe('sk_test_51GmkxsC817vZ2TBaIX4ZxnJC4E9fPo6PBVHV1TJ5IzLEmQlw9KHOQOWPfeL3wsw9gAKCEevbzYoBfcQXkVgXAfFB00KYmfqRBr');
const corsHandler = cors({origin: true});
const db = require('./firebase');
const { getDatabase } = require('firebase-admin/database');
require('dotenv').config();


functions.http('printButtonGallery', async (req, res) => {
    corsHandler(req, res, async () => {        
        console.log(req.body);

        const { selections, delivery, userInfo, payment } = req.body;
        const { address, city, country, email, name, zip } = userInfo;
        let pairs = [];

        for (const selection of selections) {
            const { size, framing, infoNft } = selection;
            const { title: nftName, description: nftCollection, id: { tokenId: nftId }} = infoNft;

            if (payment !== "fiat") continue; // Continue to next iteration if payment is not 'fiat'
            
            const product_data = {
                "title": `Print NFT - ${nftName}`,
                "description": "Print",
                "vendor": "Solid NFT",
                "product_type": "Print media",
                "size": size,
                "framing": framing
            };

            try {
                const product = await axios.post("https://us-central1-solid-certificate-v1.cloudfunctions.net/newProduct", product_data, {
                    headers: { 'Content-Type': 'application/json' }
                });

                const customer_data = {
                    "first_name": name,
                    "last_name": "test",
                    "email": email,
                    "phone": "0642424242",
                    "address1": address,
                    "city": city,
                    "province": "test",
                    "zip": zip,
                    "country": country,
                    "password": "12345",
                    "password_confirmation": "12345"
                };

                const customer = await axios.post("https://us-central1-solid-certificate-v1.cloudfunctions.net/newCustomer", customer_data, {
                    headers: { 'Content-Type': 'application/json' }
                });

                // Storing the pair of customer and product ids for each selection
                pairs.push({ customerId: customer.data.customer.id, productId: product.data.product.id });
                
            } catch (error) {
                console.error("Error sending request:", error.message);
            }
        }
        let pairsString = JSON.stringify(pairs);
        let pairsObject = Object.fromEntries(pairs.map((pair, index) => [`pair${index + 1}`, pair]));

        const id = uuid.v4().substring(0, 12);
        const product = await stripe.products.create({
            name: `Premium Print - Multiple Selections ${id}`, 
        });
        const price = await stripe.prices.create({ unit_amount: 250, currency: 'eur', product: product.id });
        const paymentLink = await stripe.paymentLinks.create({
            line_items: [{ price: price.id, quantity: selections.length }],  // Use the length of selections as quantity
            after_completion: {
                type: 'redirect',
                redirect: { url: 'https://www.solidnft.com/' },
            },
            metadata: {
                pairs: pairsString
            }
        });
        const db = getDatabase();
        const pushRef = db.ref(`/payments/${paymentLink.id}`).set({ id: paymentLink.id, status: 'not paid' });
        res.send(paymentLink);
    })
});

functions.http('printButtonUnique', async (req, res) => {
    corsHandler(req, res, async () => {
        const {
            delivery, framing, infoNft, size, 
            userInfo: { firstName, lastName, email, address, city, zip, country, phone, ethAddress }, 
            payment
        } = req.body;
        
        const { nftName, nftCollection, nftId, nftUrl, nftOwner, nftContract } = infoNft;
        
        const standardizePhone = (phone) => {
            return String(phone).replace(/[^a-zA-Z0-9]/g, '');
        }
        
        if (payment === "crypto") {
            res.json({ message: "Final amount" });
        } else if (payment === "fiat") {
            const product = await axios.post("https://us-central1-solid-certificate-v1.cloudfunctions.net/newProduct", {
                nftName,
                nftCollection,
                nftId,
                nftUrl,
                nftOwner: ethAddress,
                nftContract,
                size,
                framing,
            }, {
                headers: { 'Content-Type': 'application/json' }
            });
        
            const response = await axios.get("https://quick-start-96baaad8.myshopify.com/admin/api/2023-10/customers.json?", {
                headers: {
                    "X-Shopify-Access-Token": process.env.ACCESS_TOKEN_ADMIN,
                    'Content-Type': 'application/json'
                }
            });
            const customers = response.data.customers;
            console.log(customers)

            const standardizedInputPhone = phone?.toString()?.trim();
            const customerWithPhoneExists = customers.some(customer => standardizePhone(customer.phone) === standardizedInputPhone);
            console.log(customerWithPhoneExists)

            let customerId;
            if (!customerWithPhoneExists) {
                const customer = await axios.post("https://us-central1-solid-certificate-v1.cloudfunctions.net/newCustomer", {
                    firstName,
                    lastName,
                    email,
                    address1: address,
                    phone,
                    city,
                    province: "test",
                    zip,
                    country,
                    password: "12345",
                    password_confirmation: "12345",
                    ethAddress
                }, {
                    headers: { 'Content-Type': 'application/json' }
                });
                customerId = customer.data.customer.id;
            } else {
                customerId = customers.find(c => standardizePhone(c.phone) === standardizedInputPhone).id;
                console.log(customerId)
            }
        
            const id = uuid.v4().substring(0, 12);
            const stripeproduct = await stripe.products.create({
                name: `Premium Print - ${size} cm - ${nftName} #${nftId} ${id}`,
            });
            const price = await stripe.prices.create({ unit_amount: 250, currency: 'eur', product: stripeproduct.id });
            const paymentLink = await stripe.paymentLinks.create({
                line_items: [{ price: price.id, quantity: 1 }],
                after_completion: {
                    type: 'redirect',
                    redirect: { url: 'https://www.solidnft.com/' }
                },
                metadata: {
                    customer_id: customerId,
                    product_id: product.data.product.id
                }
            });
            const db = getDatabase();
            const pushRef = db.ref(`/payments/${paymentLink.id}`).set({ id: paymentLink.id, status: 'not paid' });
            res.send(paymentLink);
        }        
    })
});

functions.http('webhook', async (req, res) => {
    corsHandler(req, res, async () => {
        console.log(req.body)
        const db = getDatabase();
        if (req.body.type === 'checkout.session.completed') {
            const pushRef = db.ref(`/payments/${req.body.data.object.payment_link}`).set({id: req.body.data.object.payment_link, status: 'success'});
        }
        res.status(200).send();
    })
});

functions.http('checkSuccess', async (req, res) => {
    corsHandler(req, res, async () => {
        try {
          const db = admin.database();
          const paymentRef = db.ref(`/payments/${req.body.payment_id}`);
    
          // Use the 'value' event type and a callback to retrieve data
          paymentRef.once('value', (snapshot) => {
            const data = snapshot.val();
            const status = data ? data.status : null;
    
            // Prepare the response body
            const responseBody = {
              status: status
            };
    
            // Send the response with a JSON payload
            res.status(200).json(responseBody);
          });
        } catch (error) {
          console.error('Error fetching status from Firebase:', error);
          res.status(500).json({ error: 'Internal server error' });
        }
    });
})

functions.http('newCollabPrintRestriction', async (req, res) => {
    corsHandler(req, res, async () => {
        const db = getDatabase();

        const data = {
            max_prints: req.body.max_prints,
            printed: req.body.printed
        };

        await db.ref(`/CollabPrintRestriction/${req.body.nftContract}`).set(data);

        res.status(200).send();
    })
});

functions.http('updateCollabPrintRestriction', async (req, res) => {
    corsHandler(req, res, async () => {
        const db = getDatabase();
        await db.ref(`/CollabPrintRestriction/${req.body.nftContract}/${req.body.nftId}`)
        .update({
                printed: req.body.printed
            });
        res.status(200).send();
    })
});

functions.http('checkCollabPrintRestriction', async (req, res) => {
    corsHandler(req, res, async () => {


        const checkPrintability = async (collectionCount) => {
            for (const collectionName in collectionCount) {
                
                let contractAddress = collectionCount[collectionName].address;
                const nftNumber = collectionCount[collectionName].count;
                contractAddress = contractAddress.toLowerCase()

                const db = admin.database();
                const ref = db.ref(`/CollabPrintRestriction/${contractAddress}/`);
                const snapshot = await ref.once('value');
                console.log(snapshot.val())

                const data = await (await db.ref(`/CollabPrintRestriction/${contractAddress}/`).once('value')).val()

                if (!data) continue; // If no data found for a contract, continue to next one.
        
                const printed = data.printed || 0; // Defaulting to 0 if not found.
                const max_prints = data.max_prints || 0; // Defaulting to 0 if not found.
        
                if (printed + nftNumber > max_prints) {
                    return false; // This collection is not printable, return immediately.
                }
            }
            return true; // All collections are printable.
        }


        
        const collectionCount = req.body;
        const isPrintable = await checkPrintability(collectionCount);

        if (isPrintable) {
            const responseBody = {
                data: "ok"
            };
            res.status(200).json(responseBody);
        } else {
            const responseBody = {
                data: "cancel"
            };
            res.status(200).json(responseBody);
        }
    })
});

functions.http('newCheckout', async(req, res) => {
    corsHandler(req, res, async () => {
        console.log(req.body)
        console.log(process.env.ACCESS_TOKEN)
        res.status(200).send()
    })
});

functions.http('checkoutCreate', async(req, res) => {
    corsHandler(req, res, async () => {
        console.log(req.body)
        const { email, productId, address1, city, country, firstName, lastName, phone, province, zip } = req.body;
        console.log(process.env.ACCESS_TOKEN)
        const CREATE_CHECKOUT_MUTATION = `
            mutation checkoutCreate($input: CheckoutCreateInput!) {
                checkoutCreate(input: $input) {
                    checkout {
                        id
                        webUrl
                    }
                    checkoutUserErrors {
                        code
                        field
                        message
                    }
                    queueToken
                }
            }
        `;
        const variables = {
            input: {
                allowPartialAddresses: true,
                buyerIdentity: {
                    countryCode: "FR"
                },
                email: email,
                lineItems: [
                    {
                        quantity: 1,
                        variantId: `gid://shopify/ProductVariant/${productId}`
                    }
                ],
                note: "",
                shippingAddress: {
                    address1: address1,
                    address2: "",
                    city: city,
                    company: "",
                    country: country,
                    firstName: firstName,
                    lastName: lastName,
                    phone: phone,
                    province: province,
                    zip: zip
                }
            },
            queueToken: ""
        };
        try {
            const response = await axios.post('https://quick-start-96baaad8.myshopify.com/api/2023-10/graphql.json', 
            {
                query: CREATE_CHECKOUT_MUTATION,
                variables: variables
            }, 
            {
                headers: {
                    'X-Shopify-Storefront-Access-Token': process.env.ACCESS_TOKEN_V1,
                    'Content-Type': 'application/json',
                }
            })
            console.log(response)
            console.log(response.data);
            res.status(200).send(response.data);
        } catch (error) {
            console.error("Error sending request:", error.response.data);
            res.status(500).send(error.response.data);  // Send back the detailed error
        }
        res.status(200).send()
    })
});

functions.http('cartCreate', async(req, res) => {
    corsHandler(req, res, async () => {
        console.log(req.body)
        const { email, productId, address1, city, country, firstName, lastName, phone, province, zip } = req.body;
        console.log(process.env.ACCESS_TOKEN)

        const CREATE_CART_MUTATION = `
            mutation {
                cartCreate {
                    cart { 
                        # Cart fields
                    }
                    userErrors {
                        field
                        message
                    }
                }
            }
        `;

        const variables = {
            "input": {
              "attributes": [
                {
                  "key": "test",
                  "value": "value"
                }
              ],
              "buyerIdentity": {
                "countryCode": "FR",
                "customerAccessToken": "",
                "deliveryAddressPreferences": [
                  {
                    "customerAddressId": "",
                    "deliveryAddress": {
                      "address1": address1,
                      "address2": "",
                      "city": city,
                      "company": "",
                      "country": country,
                      "firstName": firstName,
                      "lastName": lastName,
                      "phone": phone,
                      "province": province,
                      "zip": zip
                    }
                  }
                ],
                "email": email,
                "phone": phone,
              },
              "lines": [
                {
                  "attributes": [
                    {
                      "key": "test",
                      "value": "value"
                    }
                  ],
                  "merchandiseId": "47261962928414",
                  "quantity": 1,
                  "sellingPlanId": ""
                }
              ],
              "metafields": [
                {
                  "key": "test",
                  "type": "single_line_text_field",
                  "value": "test"
                }
              ]
            }
        }
          
        try {
            const response = await axios.post('https://quick-start-96baaad8.myshopify.com/api/2023-10/graphql.json', 
                {
                    query: CREATE_CART_MUTATION,
                    variables: variables
                }, 
                {
                    headers: {
                        'X-Shopify-Storefront-Access-Token': process.env.ACCESS_TOKEN_V1,
                        'Content-Type': 'application/json',
                    }
                })
            console.log(response)
            console.log(response.data);
            res.status(200).send(response.data);
        } catch (error) {
            console.error("Error sending request:", error.response.data);
            res.status(500).send(error.response.data);  // Send back the detailed error
        }
        res.status(200).send()
    })
});

functions.http('newCustomer', async (req, res) => {
    corsHandler(req, res, async () => {
        const data = {
            customer: {
                first_name: req.body.firstName,
                last_name: req.body.lastName,
                email: req.body.email,
                phone: req.body.phone,
                verified_email: true,
                addresses: [
                    {
                        address1: req.body.address1,
                        city: req.body.city,
                        province: req.body.province,
                        phone: req.body.phone,
                        zip: req.body.zip,
                        last_name: req.body.lastName,
                        first_name: req.body.firstName,
                        country: req.body.country
                    }
                ],
                password: req.body.password,
                password_confirmation: req.body.password_confirmation,
                send_email_welcome: false
            }
        };
        try {
            const response = await axios.post("https://quick-start-96baaad8.myshopify.com/admin/api/2023-07/customers.json", data, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Shopify-Access-Token': process.env.ACCESS_TOKEN_ADMIN // or replace with '{access_token}' if needed
                }
            });
            console.log(response.data);
            res.status(200).send(response.data);
        } catch (error) {
            console.error("Error sending request:", error.response.data);
            res.status(500).send(error.response.data);  // Send back the detailed error
        }
    })
});

functions.http('newProduct', async (req, res) => {
    corsHandler(req, res, async () => {
        const data = {
            product: {
                title: `NFT Print - ${req.body.nftCollection}/${req.body.nftId}`,
                body_html: `
                    NFT Print:<br>
                    NFT Name: ${req.body.nftName}<br>
                    NFT Collection: ${req.body.nftCollection}<br>
                    NFT ID: ${req.body.nftId}<br>
                    NFT URL : ${req.body.nftUrl}<br>
                    NFT Owner: ${req.body.nftOwner}<br>
                    NFT Contract: ${req.body.nftContract}<br>
                    Size: ${req.body.size}<br>
                    Framing: ${req.body.framing}`,
                vendor: 'Solid NFT',
                product_type: 'Print',
                status: 'active'
            }
        };
        try {
            const response = await axios.post("https://quick-start-96baaad8.myshopify.com/admin/api/2023-07/products.json", data, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Shopify-Access-Token': process.env.ACCESS_TOKEN_ADMIN // or replace with '{access_token}' if needed
                }
            });
            console.log(response.data);
            res.status(200).send(response.data);
        } catch (error) {
            console.error("Error sending request:", error.response.data);
            res.status(500).send(error.response.data);  // Send back the detailed error
        }
        
    })
});

functions.http('newOrder', async (req, res) => {
    corsHandler(req, res, async () => {

        console.log(req.body)
        const data = {
            order: {
                line_items: [
                    {
                        title: req.body.title,
                        name: req.body.title,
                        price: 250,
                        quantity:req.body.quantity
                    }
                ],
                customer: {
                    id: req.body.customer_id
                },
                "financial_status": "paid",
                "note_attributes": [
                    {
                        "name": "delivery status",
                        "value": "not delivered"
                    }
                ],
                "total_price": `${req.body.total_price}`
            }
        };
    
        try {
            const response = await axios.post("https://quick-start-96baaad8.myshopify.com/admin/api/2023-07/orders.json", data, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Shopify-Access-Token': process.env.ACCESS_TOKEN_ADMIN // or replace with '{access_token}' if needed
                }
            });
            console.log(response.data);
            res.status(200).send(response.data);
        } catch (error) {
            console.error("Error sending request:", error.response.data);
            res.status(500).send(error.response.data);  // Send back the detailed error
        }  
    })
});

functions.http('updateOrder', async (req, res) => {
    corsHandler(req, res, async () => {
        const customerId = req.body.data.object.metadata.customer_id

        const order_data = {
            "title": `Print NFT`,
            "quantity": "1",
            "customer_id": customerId,
            "total_price": "125",
        }
        const order = await axios.post("https://us-central1-solid-certificate-v1.cloudfunctions.net/newOrder", order_data, {
            headers: {
                'Content-Type': 'application/json',
            }
        });
    })
});

functions.http('newCheckout', async (req, res) => {
    corsHandler(req, res, async () => {
        const checkout_data = {
            "checkout": {
                "line_items": 
                [
                    {
                        "variant_id":8810057335070,
                        "quantity":5
                    }
                ]
            }
        }
        const checkout = await axios.post("https://quick-start-96baaad8.myshopify.com/admin/api/2023-10/checkouts.json", checkout_data, {
            headers: {
                'Content-Type': 'application/json',
                'X-Shopify-Access-Token': process.env.ACCESS_TOKEN_ADMIN
            }
        });
        res.status(200).send(checkout.data);
    })
})